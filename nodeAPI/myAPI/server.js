var express = require('express');
var bodyParser = require('body-parser');
const { response } = require('express');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

var nbaPlayers = [
    {
        "id":"24",
        "name":"Kobe"
    },
    {
        "id":"6",
        "name":"LeBron"
    },
    {
        "id":"23",
        "name":"Jordan"
    },
    {
        "id":"7",
        "name":"KD"
    }
];

app.get('/players', function(request, response){
    response.send(nbaPlayers); // always send something back
});

app.get('/kobe', function(request, response){
    response.send('Kobe Bryant was the best Laker. Mamba Mentality!');
});

app.post('/players', function(request, response){
    var player = request.body;
    if (!player || player.name === "") {
        response.status(500).send({error:"Your player must have a name."});
    }
    else {
        nbaPlayers.push(player);
        response.status(200).send(nbaPlayers);
    }
});

app.put('/players/:playerId', function(request, response){
    
    var newPlayerName = request.body.text;

    if (!newPlayerName || newPlayerName === "") {
        response.status(500).send({error:'You must provide a player name.'});
    }
    else {
        for (var i = 0; i < nbaPlayers.length; i++) {
            if (nbaPlayers[i].id === request.params.playerId) {
                nbaPlayers[i].name = newPlayerName;
            }
            break;
        }
        console.log('NBA players list updated.');
        response.status(200).send(nbaPlayers);
    }

});

app.listen(3000, function() {
    console.log('My API is running on port 3000');
});